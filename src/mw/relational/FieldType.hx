package mw.relational;

enum FieldType {
  text(length:Int);
  int();
  blob();
  bool();
  enum_(x:Array<String>);
  haxetype(type_as_string:String, size:Int); // will be serialized
  date();
  datetime();
  currency();
}

